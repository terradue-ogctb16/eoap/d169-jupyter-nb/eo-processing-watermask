FROM terradue/l0-binder:latest

MAINTAINER Terradue S.r.l

COPY . ${HOME}
USER root

RUN /opt/anaconda/bin/conda env create --file ${HOME}/environment.yml

RUN /opt/anaconda/envs/env_hotspot/bin/python -m ipykernel install --name kernel_hotspot

RUN chown -R ${NB_UID} ${HOME}
RUN mkdir -p /workspace/data && chown -R ${NB_UID} /workspace
USER ${NB_USER}

RUN test -f ${HOME}/postBuild && ${HOME}/postBuild

ENV PREFIX /opt/anaconda/envs/env_hotspot

WORKDIR ${HOME}