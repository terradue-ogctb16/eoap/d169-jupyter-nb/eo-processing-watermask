import os
import sys
#import pandas as pd
#import otbApplication
#import lxml.etree as etree
import numpy as np
import math
from os.path import exists
import ogr
import gdal 
from osgeo.gdalconst import GA_ReadOnly
from ogr import osr
import geopandas as gpd
import json
gdal.UseExceptions()  

def polygonize(url, date, originator):
    
    ds = gdal.Open(url)
    
    srs = osr.SpatialReference(wkt=ds.GetProjection())
    
    band = ds.GetRasterBand(1)
    band_array = band.ReadAsArray()

    out_geojson = 'polygonized.json'

    driver = ogr.GetDriverByName('GeoJSON')

    out_data_source = driver.CreateDataSource(out_geojson + "")
    out_layer = out_data_source.CreateLayer('polygonized', srs=srs)

    new_field = ogr.FieldDefn('mask', ogr.OFTInteger)
    #new_field = ogr.FieldDefn('invalid', ogr.OFTInteger)
    out_layer.CreateField(new_field)

    gdal.Polygonize(band, None, out_layer, 0, [], callback=None )

    out_data_source = None
    ds = None

    data = json.loads(open(out_geojson).read())
    gdf = gpd.GeoDataFrame.from_features(data['features'])
    gdf = gdf[(gdf['mask'] == 1) | (gdf['mask'] == 254)]
    #gdf = gdf[gdf['invalid'] == 254]
    gdf['date'] = date.strftime('%Y-%m-%dT%H:%m%5s')
    gdf['originator'] = originator
    
    gdf.crs = {'init':'epsg:{}'.format(srs.GetAttrValue('AUTHORITY', 1))}
    
    os.remove(out_geojson)
    
    return gdf

    
def water_mask(s2_product, output_name, scl_product=None):
    
    gain = 10000
    
    #['B02', 'B03', 'B08', 'B11']
    ds = gdal.Open(s2_product)
    
    b02 = ds.GetRasterBand(1).ReadAsArray()
    b03 = ds.GetRasterBand(2).ReadAsArray()
    b08 = ds.GetRasterBand(3).ReadAsArray()
    b11 = ds.GetRasterBand(4).ReadAsArray()
     
    scl = None
    
    if scl_product is not None:
        
        ds_scl = gdal.Open(scl_product)
        scl = ds_scl.GetRasterBand(1).ReadAsArray() 

        ds_scl = None

        
        
    width = ds.RasterXSize
    height = ds.RasterYSize
    
    input_geotransform = ds.GetGeoTransform()
    input_georef = ds.GetProjectionRef()
    
    water_m = np.zeros((height, width), dtype=np.uint8)
    
    ndwi2 = np.zeros((height, width))
    swm = np.zeros((height, width))
    
    
    ndwi2 = (b03 / gain - b08 / gain) / (b03/ gain / gain + b08 / gain)
    swm = (b02 / gain + b03 / gain ) / (b08 /gain + b11 / gain)
    
    conditions = (swm > 1.0) | (ndwi2 > 0.3) 
    
    water_m[conditions] = 1
    
    if scl is not None:
        
        water_m[np.where((scl == 0) | (scl == 1))] = -2

   
    driver = gdal.GetDriverByName('GTiff')
    
    output = driver.Create(output_name, 
                           width, 
                           height, 
                           1, 
                           gdal.GDT_Byte) #Float32)
        
    output.SetGeoTransform(input_geotransform)
    output.SetProjection(input_georef)
    output.GetRasterBand(1).WriteArray(water_m),

    output.FlushCache()
    
    return True



    
    

    